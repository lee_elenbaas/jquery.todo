/**
 * @author lee elenbaas
 */
(function($){
	"use strict";
	
	/////// Model //////////
	
	var GeneralTask = (function(){
		function GeneralTask(title, description, priority) {
			this.type = 'GeneralTask';
			this.title = title;
			this.description = description;
			this.priority = priority;
		}
		
		GeneralTask.prototype = {
			dueHours: function () {
				return Infinity;
			}
		};
		
		return GeneralTask;
	})();
	
	var ToDoTask = (function(){
		function ToDoTask(title, description, priority, dueDate) {
			GeneralTask.call(this, title, description, priority);
			this.dueDate = dueDate;
			this.type = 'ToDoTask';
		}
		
		ToDoTask.prototype = {
			dueHours: function() {
				if (this.dueDate) {
					var now = Math.floor(new Date().getTime() / 1000 / 60 / 60);
					var due = Math.floor(this.dueDate.getTime() / 1000 / 60 / 60);
					
					return due - now;
				}
				return Infinity;
			}
		};
		
		ToDoTask.prototype.prototype = GeneralTask.prototype; 
		
		return ToDoTask;
	})();
	
	////////// Storage ///////////	
	
	var StorageHandler = (function()
	{
		function StorageHandler(storageKey) {
			this.storageKey = storageKey.toString();
			this.cachedData = false;
		}
			
		StorageHandler.prototype = {
			load: function(ignoreCache) {
				if ((this.cachedData !== false) && !ignoreCache)
					return this.cachedData;
					
				var saved = $.parseJSON(localStorage.getItem(this.storageKey));
				
				if (!$.isArray(saved))
					return [];

				var todos = [];
				
				// link each stored task back to its class
				saved.forEach(function(todo){
					var type = eval(todo.type);
					
					if (typeof type === 'function') {
						todo = $.extend(new type(),todo);
						
						if ((typeof todo.dueDate === "string") && todo.dueDate)
								todo.dueDate = new Date(todo.dueDate);
							
						todos.push(todo);
					}
				});
				
				this.cachedData = todos;
				
				return this.cachedData;
			},
			save: function(todos) {
				this.cachedData = todos;
				
				localStorage.setItem(this.storageKey, JSON.stringify(todos));
			}
		};
		
		return StorageHandler;
	})();
	
	/////////// Default templates //////////////
	
	function listView(todos, itemHandler) {
		var controller = this;
		var html = '<div>'
		
		todos.forEach(function(todo, i){
			var type = todo.type || controller.defaultType;
			var view = controller.views[type] && controller.views[type].listItemView;
			
			if (typeof view !== 'function') 
				view = controller.views[controller.defaultType].listItemView;
				
			var css = controller.styleSelector(todo);
			html += '<div data-todo-item="'+i+'"'+(css?' class="'+css+'"':'')+'>'+view(todo)+'</div>';
		});
		
		html += '</div>'
		
		return html;
	}
	
	function styleSelector(todo) {
		var dueHours = todo.dueHours();
		
		if (dueHours === Infinity)
			return 'todo-timeless';
			
		if (dueHours > 24)
			return 'todo-far-future';			
		if (dueHours > 1)
			return 'todo-near-future';
		if (dueHours >= 0)
			return 'todo-present';
		
		return 'todo-past';
	}
	
	function todoListItemView(todo) {
		return '<input class="todo-completed" type="checkbox"'+(todo.completed?' checked="checked"':'')+' /><span class="title">'+todo.title+'</span><span class="dueDate">'+todo.dueDate+'</span><button class="todo-select">...</button>';
	}
	
	function todoFullItemView(todo) {
		return ''
			+ '<div><label>Title: <input class="todo-title" value="'+todo.title+'" /></label></div>'
			+ '<div><label><input class="todo-completed" type="checkbox"'+(todo.completed?' checked="checked"':'')+' /> Completed</label>'
			+ '<label>Priority: <input class="todo-priority" type="number" value="'+todo.priority+'" /></label></div>'
			+ '<div><label>Due date: <input class="todo-dueDate" type="date" value="'+(todo.dueDate?todo.dueDate.getFullYear()+'-'+(todo.dueDate.getMonth()+1)+'-'+todo.dueDate.getDate():'')+'" /></label>'
			+ ' <label>time: <input class="todo-dueTime" type="time" value="'+(todo.dueDate?todo.dueDate.getHours()+':'+todo.dueDate.getMinutes():'')+'" /></label></div>'
			+ '<div><textarea class="todo-description">'+todo.description+'</textarea></div>'
			+ '<div><button class="todo-update">Save</button><button class="todo-delete">Delete</button><button class="todo-list">Back</button></div>'
	}

	function generalListItemView(todo) {
		return '<input class="todo-completed" type="checkbox"'+(todo.completed?' checked="checked"':'')+' /><span class="title">'+todo.title+'</span><button class="todo-select">...</button>';
	}
	
	function generalFullItemView(todo) {
		return ''
			+ '<div><label>Title: <input class="todo-title" value="'+todo.title+'" /></label></div>'
			+ '<div><label><input class="todo-completed" type="checkbox"'+(todo.completed?' checked="checked"':'')+' /> Completed</label>'
			+ '<label>Priority: <input class="todo-priority" type="number" value="'+todo.priority+'" /></label></div>'
			+ '<div><textarea class="todo-description">'+todo.description+'</textarea></div>'
			+ '<div><button class="todo-update">Save</button><button class="todo-delete">Delete</button><button class="todo-list">Back</button></div>'
	}
	
	function initialItemViews() {
		return {
			GeneralTask:{
				listItemView: generalListItemView,
				fullItemView: generalFullItemView
			},
			ToDoTask:{
				listItemView: todoListItemView,
				fullItemView: todoFullItemView
			}
		};
	}
	
	function formView(todos) {
		var html = '<div>'
			+ '<input type="text" class="todo-new-title">'
			+ '<input class="todo-new-dueDate" type="date" />'
			+ '<button class="todo-add">Add</button></div>'
		
		html += this.listView(todos);
		
		return html;
	}
	
	//////////// List order //////////////////
	
	function priorityComparer(todo1, todo2) {
		return todo2.priority - todo1.priority;
	}
	
	///////////// Controller /////////////////
	
	var ToDoController = (function(){
		function ToDoController(storageKey, elements) {
			// controller initiation values
			this.elements = elements;
			this.storageHandler = new StorageHandler(storageKey);
			
			// views
			this.formView = formView;
			this.listView = listView;
			this.views = initialItemViews();
			this.comparer = priorityComparer;
			this.styleSelector = styleSelector;
			this.defaultType = 'ToDoTask';
			
			// data
			this.selectedTaskIndex = false;
			this.defaultPriority = 5;
			
			// control logic
			
			var controller = this; // access to the controller form inside functions
			
			// automatic refresh by polling
			// TODO: refresh by timeout rather then interval
			var intervalId = window.setInterval(function() {
				this.render();
			}, 15*60*1000);
			
			// event handlers
			
			elements.on('click', '.todo-add', function() {
				var title = elements.find('.todo-new-title').val() || '';
				var description = elements.find('.todo-new-description').val() || '';
				var priority = elements.find('.todo-new-priority').val() || controller.defaultPriority;
				
				var dueDate = elements.find('.todo-new-dueDate').val();
				if (dueDate !== '')
					dueDate = new Date(dueDate);
				else
					dueDate = '';
				
				if (title)
					controller.addTask(new ToDoTask(title, description, priority, dueDate))
					
				return false;
			});
			elements.on('click', '.todo-completed', function(e) {
				var todoView = $(e.target).closest('[data-todo-item]');
				
				var index = todoView.data('todo-item');
				
				controller.complete(index, !task.completed);
				return false;
			});
			elements.on('click', '.todo-select', function(e) {
				var todoView = $(e.target).closest('[data-todo-item]');
				
				var index = todoView.data('todo-item');
				
				controller.showTask(index);
			});
			elements.on('click', '.todo-list', function() {
				controller.showTask(false);
			});
			elements.on('click', '.todo-update', function() {
				controller.updateSelectedTask();
				controller.showTask(false);
			});
			elements.on('click', '.todo-delete', function() {
				controller.deleteTask(controller.selectedTaskIndex);
			});
		}
		
		// controller logic
		ToDoController.prototype = {
			render: function() {
				var html;
				var todos = this.storageHandler.load();
				
				if (this.selectedTaskIndex !== false) {
					var selectedTask = todos[this.selectedTaskIndex];
					
					var type = selectedTask.type || this.defaultType;
					var view = this.views[type] && this.views[type].fullItemView;
					
					if (typeof view !== 'function') 
						view = this.views[this.defaultType].fullItemView;
						
					html = view(selectedTask);
				}
				else { // render list
					todos = todos.sort(this.comparer);
					
					html = this.formView(todos);
				}

				this.elements.html(html);
			},
			deleteTask: function(task) {
				var taskIndex;
				var todos = this.storageHandler.load();

				if (todos.some(function(t,i) {
					if (t === task) {
						taskIndex = i;
						return true;
					}
					return false;
				})) {
					todos.splice(taskIndex,1);
					
					if (this.selectedTaskIndex == taskIndex)
						this.selectedTaskIndex = false;

					this.storageHandler.save(todos);
					this.render();
				}
			},
			addTask: function(task) {
				// TODO: need to check that it is a valid task type
				var todos = this.storageHandler.load();
				todos.push(task);
				
				this.storageHandler.save(todos);
				this.render();
			},
			getTask: function(index) {
				var todos = this.storageHandler.load();
				return todos[index];
			},
			complete: function(index, completed) {
				var todos = this.storageHandler.load();
				var task = todos[index];
				task.completed = completed;
				
				this.storageHandler.save(todos);
				this.render();
			},
			updateSelectedTask: function() {
				var todos = this.storageHandler.load();
				var selectedTask = todos[this.selectedTaskIndex];
				
				selectedTask.title = this.elements.find('.todo-title').val();
				selectedTask.description = this.elements.find('.todo-description').val();
				selectedTask.completed = (this.elements.find('.todo-completed').first().filter(':checked').length > 0);
				selectedTask.priority = this.elements.find('.todo-priority').val();
				
				var dueDate = this.elements.find('.todo-dueDate').val()+' '+this.elements.find('.todo-dueTime').val();
				
				if (dueDate.trim())
					selectedTask.dueDate = new Date(dueDate);
				else
					selectedTask.dueDate = '';
				
				this.storageHandler.save(todos);
			},
			showTask: function(index) {
				this.selectedTaskIndex = index;
				
				this.render();
			}
		};
		
		return ToDoController;
	})();
	
	// expose API through jQuery
	
	$.todo = {
		GeneralTask: GeneralTask,
		ToDoTask: ToDoTask,
		StorageHandler: StorageHandler
	};
	
	$.fn.todo = function(storage) {
		if (!storage || (typeof storage.load !== 'function') || (typeof storage.save !== 'function'))
			storage = new StorageHandler(storage);

		var controller = new ToDoController(storage, this);
		
		controller.render(); // render the todo control
		
		this.data('todo', controller); // make controller accessible later through jquery
		
		return this; // for jquery chaining
	};
	
})(jQuery);
